help:                                                                           ## shows this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_\-\.]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

phpunit:                                                                        ## run phpunit tests
	vendor/bin/phpunit --testdox -v --colors="always" $(OPTIONS)

coverage:                                                                       ## run phpunit tests with coverage
	xdebug vendor/bin/phpunit --testdox -v --colors="always" --coverage-html coverage $(OPTIONS)

ci-coverage:                                                                    ## run phpunit tests with coverage on ci
	vendor/bin/phpunit --testdox -v --colors="never" --coverage-text $(OPTIONS)

phpstan:                                                                        ## run static code analyser
	phpstan analyse src -l max

php-cs-check:																	## run cs fixer (dry-run)
	PHP_CS_FIXER_FUTURE_MODE=1 php-cs-fixer fix --allow-risky=yes --diff --dry-run

php-cs-fix:																		## run cs fixer
	PHP_CS_FIXER_FUTURE_MODE=1 php-cs-fixer fix --allow-risky=yes

psalm:																			## run psalm
	psalm

ci-psalm:																		## run psalm for ci
	psalm --show-info=false

cache:																			## clear and warm up symfony cache
	bin/console ca:cl && bin/console ca:wa

mysql:                                                                          ## go in mysql
	sudo docker exec -it mysql /usr/bin/mysql -u root -pgeheim app

js:                                                                             ## init yarn encore stuff
	yarn install
	yarn run dev

init:																			## initialize project
	composer install
	make reset-db
	make js
	make cache

reset-db:                                                                       ## reset database
	bin/console doctrine:database:drop --force --if-exists
	bin/console doctrine:database:create
	bin/console doctrine:migration:migrate --no-interaction
	bin/console doctrine:schema:validate
	bin/console hautelook:fixtures:load --no-interaction

security:
	security-checker security:check

dev-check: phpstan psalm php-cs-check security check-dependencies phpunit                  ## run dev checks

.PHONY: cache phpstan psalm phpunit coverage php-cs-check security php-cs-fix help dev-check init
