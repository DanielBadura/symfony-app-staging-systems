FROM composer as php

ARG APP_ENV=dev
ARG COMPOSER_ALLOW_SUPERUSER=1

# for cache clear
ARG DATABASE_URL=mysql://root:geheim@mysql:3306/app?charset=utf8mb4&serverVersion=5.7

WORKDIR /var/build
COPY . .

# cleanup
RUN rm -Rf vendor docker tests var
RUN mkdir var

# install packages + run post-install scripts like `cache:warmup`
RUN composer install

#####

FROM node:11-alpine as assets

WORKDIR /var/build
COPY . .

RUN yarn install
RUN yarn run dev

#####

FROM phusion/baseimage:0.11

ARG DEBIAN_FRONTEND=noninteractive

# set environments
RUN echo dev > /etc/container_environment/APP_ENV
RUN echo Europe/Berlin > /etc/container_environment/TZ

# install common tools & tzdata
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        software-properties-common \
        tzdata \
        && \
    rm -r /var/lib/apt/lists/*

# install php
RUN LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        php7.3 \
        php7.3-fpm \
        php7.3-cli \
        php7.3-intl \
        php7.3-pdo \
        php7.3-zip \
        php7.3-xml \
        php7.3-mbstring \
        php7.3-json \
        php7.3-curl \
        php7.3-pdo \
        php7.3-mysql \
        php7.3-opcache \
        php7.3-apcu \
        php7.3-gd \
        && \
    rm -r /var/lib/apt/lists/*

# configurate php
RUN echo "\
error_reporting = -1\n \
display_errors = On\n \
display_startup_errors = On\n \
upload_max_filesize = 100M\n \
memory_limit = 1024M\n \
date.timezone = Europe/Berlin\n" \
> /etc/php/7.3/fpm/conf.d/99-app.ini

# install & setup nginx
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        nginx \
        && \
    rm -r /var/lib/apt/lists/*

# setup php-fpm
RUN mkdir /etc/service/php-fpm
COPY docker/php-fpm.sh /etc/service/php-fpm/run
RUN chmod +x /etc/service/php-fpm/run

RUN mkdir /run/php
COPY docker/fpm-www.conf /etc/php/7.3/fpm/pool.d/www.conf

# setup nginx
RUN mkdir /etc/service/nginx
COPY docker/nginx.sh /etc/service/nginx/run
RUN chmod +x /etc/service/nginx/run

COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/nginx.vhost /etc/nginx/vhosts.d/app.conf

# setup boot script
RUN mkdir -p /etc/my_init.d
COPY docker/boot.sh /etc/my_init.d/boot.sh
RUN chmod +x /etc/my_init.d/boot.sh

# copy project
COPY --from=php /var/build /srv/share
COPY --from=assets /var/build/public/build/ /srv/share/public/build/

RUN chown -R www-data:www-data /srv/share/var

EXPOSE 80

CMD ["/sbin/my_init"]
